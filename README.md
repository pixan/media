# Pixan/Media #

Pixan media nos permite comenzar a trabajar con imágenes rápidamente en nuestro proyecto.


## MediaController ##

El paquete provee el controlador MediaController, el cual expone los métodos más comunes para la manipulación de media.

## Media routes ##

Es necesario agregar las siguientes rutas al proyecto para hacer uso de los tres métodos básicos del **MediaController**: create, read y destroy
```
#!php

Route::post('media', '\Pixan\Media\Controllers\MediaController@store');
Route::get('media/{id}-{filename}', '\Pixan\Media\Controllers\MediaController@show');
Route::delete('media/{id}-{filename}', '\Pixan\Media\Controllers\MediaController@destroy');
```


## Subir imágenes utilizando el controlador MediaController ##

A travez del MediaController podemos definir un solo punto para la subida de imágenes, la ruta:

```
#!php

Route::post('media', '\Pixan\Media\Controllers\MediaController@store');
```

expone en **/media** el método para subir una o multiples imágenes.

**Ejemplo de respuesta**
```
#!json

{
  "status": "ok",
  "code": 200,
  "messages": [],
  "data": [
    {
      "filename": "7f5fc4d0-b5da-11e6-92bd-7bceaa956e04.jpg",
      "media_type": "image/jpeg",
      "updated_at": "2016-11-29 02:21:12",
      "created_at": "2016-11-29 02:21:12",
      "id": 14
    }
  ]
}
```


## Instalación ##
Agregar en composer.json **"pixan/media": "dev-master"**

```
#!json
{
    "require": {
		"laravel/framework": "5.0.*",
		"pixan/media": "dev-master"
    }
}
```

Ejecutar composer update para descargar el paquete

```
#!shell

composer update
```

En el archivo **config/app.php** en el array de **providers** agregar


```
#!php

'Pixan\Media\MediaServiceProvider'

```

Publicar la **configuración** del paquete

```
#!shell

php artisan vendor:publish
```

Correr las **migraciones**


```
#!shell

php artisan migrate
```


# TODO #
* Create methods to save media with base 64 string.
* Separate package api, separate media logic from api logic.