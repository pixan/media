<?php

namespace Pixan\Media\Services;

use Pixan\Media\Models\Media;
use Intervention\Image\ImageManagerStatic as Image;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class MediaService
{

    public function store($media)
    {
        $result = [];
        foreach ($media as $mediaFile) {
            $newFileName = Uuid::generate() . '.' . $mediaFile->getClientOriginalExtension();
            $mimeType = $mediaFile->getMimeType();
            // update in db
            $newMedia = new Media([
                'filename' => $newFileName,
                'media_type' => $mimeType
            ]);
            $newMedia->save();
            // get folder path
            $folderPath = pathForAssetWithId($media->id);
            Storage::put($folderPath . $newFileName, File::get($mediaFile));
            $result[] = $newMedia;
        }
        return $result;
    }

    public function getImage($id, $filename, $width, $height)
    {
        $media = Media::findOrFail($id);
        if ($media->filename != $filename) {
            $media = null;
        }

        // get folder path
        $folderPath = pathForAssetWithId($media->id);

        if (Storage::exists($folderPath . $filename)) {
            $img = Image::make(base64_encode(Storage::get($folderPath . $filename)));
            if ($width && !$height) {
                $img = $img->widen($width, function ($constraint) {
                    $constraint->upsize();
                });
            } else if (!$width && $height) {
                $img = $img->heighten($height, function ($constraint) {
                    $constraint->upsize();
                });
            } else if ($width && $height) {
                $img = $img->fit($width, $height, function ($constraint) {
                    $constraint->upsize();
                });
            }
            $media = $img;
        } else {
            $media = null;
        }

        return $media;
    }

    public function destroy($media)
    {
        // get folder path
        $folderPath = pathForAssetWithId($media->id);
        if (Storage::exists($folderPath . $media->filename)) {
            // Delete file
            Storage::delete($folderPath . $media->filename);
            // delete model
            return $media->delete();
        } else {
            return false;
        }
    }
}