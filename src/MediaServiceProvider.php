<?php namespace Pixan\Media;

use Illuminate\Support\ServiceProvider;

class MediaServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/migrations' => database_path('/migrations')
        ]);

        if (!$this->app->routesAreCached()) {
            require __DIR__ . '/routes.php';
        }

        $this->app->make('Pixan\Media\Controllers\MediaController');
        $this->app->make('Pixan\Media\Models\Media');
        $this->registerHelpers();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
    }

    public function registerHelpers()
    {
        // Load the helpers in app/Http/helpers.php
        if (file_exists($file = __DIR__ . '/helpers/helpers.php')) {
            require $file;
        }
    }
}
