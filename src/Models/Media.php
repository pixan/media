<?php

namespace Pixan\Media\Models;

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Media extends Eloquent {
    protected $fillable = ['media_type', 'filename'];
}