<?php
namespace Pixan\Media\Traits;

use Illuminate\Support\Facades\Input;
use Pixan\Media\Models\Media;
use Pixan\Api\Controllers\ApiController;
use Webpatser\Uuid\Uuid;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Pixan\Media\Services\MediaService;

trait ManageMedia
{

    protected $apiController, $mediaService;

    public function __construct(ApiController $apiController, MediaService $mediaService)
    {
        $this->apiController = $apiController;
        $this->mediaService = $mediaService;
    }

    public function store()
    {
        $allMedia = Input::file();
        return $this->apiController->respondCreated($this->mediaService->store( ));
    }

    public function show($id, $filename)
    {
        // get media wihth id
        $media = Media::findOrFail($id);

        // get folder path
        $folderPath = pathForAssetWithId($media->id);

        if (Storage::exists($folderPath . $filename)) {
            $img = Image::make(base64_encode(Storage::get($folderPath . $filename)));
            $w = Input::get('w', false);
            $h = Input::get('h', false);
            if ($w && !$h) {
                $img = $img->widen($w, function ($constraint) {
                    $constraint->upsize();
                });
            } else if (!$w && $h) {
                $img = $img->heighten($h, function ($constraint) {
                    $constraint->upsize();
                });
            } else if ($w && $h) {
                $img = $img->fit($w, $h, function ($constraint) {
                    $constraint->upsize();
                });
            }
            return $img->response();
        } else {
            return $this->apiController->respondNotFound();
        }
    }

    public function destroy($id, $filename)
    {
        // get media wihth id
        $media = Media::findOrFail($id);
        // get folder path
        $folderPath = pathForAssetWithId($media->id);
        if (Storage::exists($folderPath . $filename)) {
            // Delete file
            Storage::delete($folderPath . $filename);
            // delete model
            $media->delete();
            return $this->apiController->respondWithData(null);
        } else {
            return $this->apiController->respondNotFound();
        }
    }

}