<?php

namespace Pixan\Media\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Pixan\Media\Traits\ManageMedia;

class MediaController extends BaseController
{
    use ManageMedia;
}
